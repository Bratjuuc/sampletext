{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module RelativePrecission where

import PrecissionClass
import Data.Bifunctor (Bifunctor(first))

newtype RPrec a = RPrec {getRPrec :: (a, Double)} deriving Eq

instance (Precission (RPrec a) a Double) where
    getVal = fst . getRPrec
    getPrec = snd . getRPrec
    fmap' f = RPrec . first f . getRPrec

instance Num (RPrec Double) where
     RPrec (x1, p1) + RPrec (x2, p2) = RPrec (x1 + x2, (p1 * abs x1 + p2 * abs x2 + p1 * p2 * x1 * x2)/ x1 + x2)
     RPrec (x1, p1) * RPrec (x2, p2) = RPrec (x1 * x2, p1 + p2)
     abs = fmap' abs
     signum = fmap' signum
     fromInteger int = RPrec (fromInteger int, 0)
     negate = fmap' negate

instance Fractional (RPrec Double) where
     RPrec (x1, p1) / RPrec (x2, p2) = RPrec (x1 / x2, p1 + p2)
     fromRational r = RPrec (fromRational r, 0)

instance Show a => Show (RPrec a) where
    show (RPrec (a,b)) = show a ++ " +- " ++ show b ++ " %"