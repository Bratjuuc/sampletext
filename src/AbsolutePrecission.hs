{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module AbsolutePrecission where

import PrecissionClass
import Data.Bifunctor (Bifunctor(first))

newtype APrec a = APrec {getAPrec :: (a, a)} deriving Eq

instance (Precission (APrec a) a a) where
    getVal = fst . getAPrec
    getPrec = snd . getAPrec
    fmap' f = APrec . first f . getAPrec

instance Num a => Num (APrec a) where
     APrec (x1, p1) + APrec (x2, p2) =  APrec (x1 + x2, p1 + p2)
     APrec (x1, p1) * APrec (x2, p2) =  APrec (x1 * x2, abs x1 * p2 + abs x2 * p1 + p1*p2)
     abs = fmap' abs
     signum = fmap' signum
     fromInteger int = APrec (fromInteger int, 0)
     negate = fmap' negate

instance Fractional a => (Fractional (APrec a)) where
     APrec (x1, p1) / APrec (x2, p2) = APrec (x1 / x2, (abs x1 * p2 + abs x2 * p1 + p1*p2) / x2 ^ 2)
     fromRational r = APrec (fromRational r, 0)

instance (Show a) => Show (APrec a) where
    show (APrec (a,b)) = show a ++ " +- " ++ show b