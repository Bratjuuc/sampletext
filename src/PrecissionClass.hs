{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module PrecissionClass where

class Precission p a pr | p -> a, p -> pr where
    getVal :: p -> a
    getPrec :: p -> pr
    fmap' :: (a -> a) -> p -> p