module Conversion where

import AbsolutePrecission
import RelativePrecission

fromAbs :: APrec Double -> RPrec Double
fromAbs (APrec (x, p)) = RPrec (x, p / abs x)

fromRel :: RPrec Double -> APrec Double
fromRel (RPrec (x, p)) = APrec (x, abs $ p * x)