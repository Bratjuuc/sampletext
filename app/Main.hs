module Main where

import AbsolutePrecission ( APrec(APrec) )
import PrecissionClass
import Data.Foldable (traverse_)

x1 :: APrec Double
x1 = APrec (2, 0.005)

x2 :: APrec Double
x2 = APrec (7, 0.04)

main :: IO ()
main = traverse_ print $ map (($ x2) . ($ x1)) [(+), (-), (*), (/)]
